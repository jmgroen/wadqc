import os
import logging

def reset_logging(log_name):
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    for filt in logging.root.filters[:]:
        logging.root.removeFilter(filt)

    logging.basicConfig(format='[%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=getattr(logging, log_name))

def multidict(inputdict, ncopies, ignore = []):
    # return a list of distinguishable copies of the same input dict
    output = []
    for i in range(ncopies):
        newdict = inputdict.copy()
        for key, value in newdict.items():
            if not key in ignore and isinstance(value, str):
                newdict[key] += '_'+str(i).zfill(2)
        output.append(newdict)
    return output
    
def dbfreshstart(dbio, inifile, setupfile):
    # truncates all tables from the database, ready for a fresh start
    try:
        dbio.db_connect(inifile)
        dbio.db_truncate(setupfile)
    except Exception as e:
        logging.info(' '.join(["Could not empty database, assuming it does not exist, so create it now",str(e)]))
        dbio.db_create_only(inifile, setupfile)
        dbio.db_connect(inifile)


