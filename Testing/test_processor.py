#!/usr/bin/env python
from __future__ import print_function

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import unittest
import subprocess
import time
import signal
import peewee

from wad_qc.connection.pacsio import PACSIO
from wad_qc.connection import dbio

os.chdir(os.path.dirname(os.path.abspath(__file__)))

setupfile = 'wadsetup.ini'
inifile = 'wadconfig.ini'

try:
    dbio.db_connect(inifile)
    dbio.db_truncate(setupfile)
except peewee.OperationalError:
    dbio.db_create_only(inifile, setupfile)
    dbio.db_connect(inifile)

    
dcm_folder = "Some_dicom_data"
config = {
        'name': 'WADQC',
        'typename': 'orthanc',
        'aetitle': 'WADQC',
        'protocol': 'http',
        'host': 'localhost',
        'port': 8055,
        'user':'orthanc',
        'pswd': 'waddemo'
    }
pacsio = PACSIO(config)
logs = pacsio.uploadDicomFolder(dcm_folder)
instID = logs[0]['ID']
studyID = pacsio.getStudyId(instanceid=instID)
seriesID = pacsio.getSeriesIds(studyID)[0]

some_module = {
    'name':'module_test',
    'uploadfilepath':'Some_modules/TestModule/testmodule.py',
    'filename': 'testmodule.py',
    'description':'test module',
}
some_config = {
    'modulename': some_module['name'],
    'name':'config_test',
    'description':'test config',
    'datatypename':'dcm_series',
}
with open("Configs/testmodule_some_config.json") as f:
    some_config['val'] = f.read()

some_selector = {
    'configname': some_config['name'], 
    'configversion': 1,
    'name': 'selector_test', 
    'description': 'test selector',
    'datatypename': 'dcm_series',
    'hidden': True,
    'frequency': 12,
}
some_process = {
    'selectorname': some_selector['name'],
    'datasourcename': 'WADQC',
    'data_id': seriesID,
    'processstatusname':'new'
}


module = dbio.DBModules.create(**some_module)
config = dbio.DBModuleConfigs.create(**some_config)
selector = dbio.DBSelectors.create(**some_selector)        
process = dbio.DBProcesses.create(**some_process)

print(module.filename)
print(config.val)
print(process.process_status.name)



