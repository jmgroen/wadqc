#!/usr/bin/env python
from __future__ import print_function
"""
stand alone test for all functions in dbio

Changelog:
  20160610: updated to match changes in dbio
  20160606: split inifile in dbconfig and setup
  20160531: moved dictionaries for definitions of items to separate file for reusage
  20160530: new dbio
  20160520: version for peewee; added testing of addrule_values, deleterule_values, addprocesslog, 
            set processstatus, getDataTypeName, getSelectors, getProcess, getProcesses2, getProcessIDs;
            removed test output
  20160518: removed uniqueness of module_config name, else we cannot do versioning
  20160513: python3 compatible; simplified logger
  20160426: implement multi-value for rule
  20160425: prefer dbio init by dict
  20160420: renamed relation to logic; adding data_type (series, etc.)
"""

__version__ = '20160610'

import logging

import os
try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.connection import dbio
    logging.info("** using installed package wad_qc **")
except ImportError: 
    import sys
    # add parent folder to search path for modules
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from wad_qc.connection import dbio
    logging.info("** development mode **")

from wad_qc.connection import bytes_as_string
import test_base

def make_testdb(modules, confdict, seldict, ruledict, json_results):
    logging.info('    Add %d modules'%(len(modules)))
    mod_ids = []
    for mod in modules:
        mod_ids.append(dbio.DBModules.create(**mod))

    logging.info('    Add config')
    conf_id = dbio.DBModuleConfigs.create(**confdict)

    logging.info('    Add selector')
    sel_id = dbio.DBSelectors.create(**seldict)

    logging.info('    Add rule')
    rule_id = sel_id.addRule(**ruledict)

    logging.info('    Add 3 processes')
    for i in range(3):
        proc_id = dbio.DBProcesses.create(**{
            'selector':sel_id,
            'data_id': 'not_a_study_id',
            'processstatusname':'new',
            'datasourcename': 'WADQC'
             })

    res_id = dbio.DBResults.finishedProcess(proc_id)

    logging.info('    Store results')
    for res in json_results:
        cat = res['category']
        field_dict = res.copy()
        del field_dict['category']
        field_dict['result'] = res_id
        if cat == 'string':
            dbio.DBResultStrings.create(**field_dict)
        elif cat == 'float':
            dbio.DBResultFloats.create(**field_dict)
        elif cat == 'bool':
            dbio.DBResultBools.create(**field_dict)
        elif cat == 'object':
            value = None
            with open(res['val'],'rb') as f:
                value = bytes(f.read())
            field_dict['val'] = bytes_as_string(value)

            dbio.DBResultObjects.create(**field_dict)
        
def test_module(modules):
    logging.info('  Testing modules')

    logging.info('    Add %d modules'%(len(modules)))
    mod_ids = []
    for mod in modules:
        mod_ids.append(dbio.DBModules.create(**mod))

    logging.info('    Add first module again')
    # test uniqueness of names
    try:
        dbio.DBModules.create(**modules[0])#upload=False
    except Exception as e:
        logging.info(' '.join(['      Good: Failed to insert same module twice,',str(e)]))

    
    logging.info('    Delete module by name')
    dbio.DBModules.get_by_name(modules[0]['name']).delete_instance()

    logging.info('    Delete module by pk')
    dbio.DBModules.get_by_id(mod_ids[1]).delete_instance()
    

    logging.info('    Deleting remaining modules')
    num = 0
    for mod in dbio.DBModules.select():
        num += 1
        mod.delete_instance()
    if num == len(modules)-2:
        logging.info('      Good: Deleted %d (%d) uploaded modules'%(num,len(modules)-2))
    else:
        logging.info('      ERROR! Deleted %d (%d) uploaded modules'%(num,len(modules)-2))
    
def test_config(modules, confdict):
    logging.info('  Testing config')

    logging.info('    Add module')
    mod_id = dbio.DBModules.create(**modules[0])#upload=False

    logging.info('    Add config')
    conf_id = dbio.DBModuleConfigs.create(**confdict)

    logging.info('    Add config again')
    # test uniqueness of names
    try:
        dbio.DBModuleConfigs.create(**confdict)
        logging.info(' '.join(['      Good: Can insert same config twice,']))
    except Exception as e:
        logging.info(' '.join(['      Error: Failed to insert same config twice,',str(e)]))

    logging.info('    Delete configs by name')
    dbio.DBModuleConfigs.get_by_name(confdict['name']).delete_instance()

    logging.info('    Delete config by pk')
    conf_id = dbio.DBModuleConfigs.create(**confdict)
    dbio.DBModuleConfigs.get_by_id(conf_id).delete_instance()

    logging.info('    Delete module')
    dbio.DBModules.get_by_id(mod_id).delete_instance()

    
def test_selector(modules, confdict, seldict):
    logging.info('  Testing selector')

    logging.info('    Add module')
    mod_id = dbio.DBModules.create(**modules[0])#upload=False

    logging.info('    Add config')
    conf_id = dbio.DBModuleConfigs.create(**confdict)

    logging.info('    Add selector')
    sel_id = dbio.DBSelectors.create(**seldict)

    logging.info('    Add selector again')
    # test uniqueness of names
    try:
        dbio.DBSelectors.create(**seldict)
    except Exception as e:
        logging.info(' '.join(['      Good: Failed to insert same selector twice,',str(e)]))

    logging.info('    Delete selector by name')
    dbio.DBSelectors.get_by_name(seldict['name']).delete_instance()

    logging.info('    Delete selector by pk')
    sel_id = dbio.DBSelectors.create(**seldict)
    dbio.DBSelectors.get_by_id(sel_id).delete_instance()

    logging.info('    Delete config by pk')
    dbio.DBModuleConfigs.get_by_id(conf_id).delete_instance()

    logging.info('    Delete module by pk')
    dbio.DBModules.get_by_id(mod_id).delete_instance()

def test_rule(modules, confdict, seldict, ruledict):
    logging.info('  Testing rule')

    logging.info('    Add module')
    mod_id = dbio.DBModules.create(**modules[0])#upload=False

    logging.info('    Add config')
    conf_id = dbio.DBModuleConfigs.create(**confdict)

    logging.info('    Add selector')
    sel_id = dbio.DBSelectors.create(**seldict)

    logging.info('    Add rule')
    rule_id = sel_id.addRule(**ruledict)

    logging.info('    Add rule again')
    # test uniqueness of names
    try:
        sel_id.addRule(**ruledict)
        logging.info('      Good: added the same rule twice,')
        
    except Exception as e:
        logging.info(' '.join(['      Error: Failed to insert same rule twice,',str(e)]))

    logging.info('    Add rule_values')
    
    rule_value_ids = [dbio.DBRuleValues.create(val=rv, rule=rule_id).id for rv in ['xone','xtwo','xthree']]

    logging.info('    Get selectors')
    sels = dbio.DBSelectors.select()
    logging.info('      Found %d selectors'%(len(sels)))
    
    logging.info('    Delete %d rule_values'%(len(rule_value_ids)))
    for rvid in rule_value_ids:
        dbio.DBRuleValues.get_by_id(rvid).delete_instance()

    logging.info('    Delete rule by pk')
    dbio.DBSelectorRules.get_by_id(rule_id).delete_instance()

    logging.info('    Delete selector by pk')
    dbio.DBSelectors.get_by_id(sel_id).delete_instance()

    logging.info('    Delete config by pk')
    dbio.DBModuleConfigs.get_by_id(conf_id).delete_instance()

    logging.info('    Delete module by pk')
    dbio.DBModules.get_by_id(mod_id).delete_instance()

def test_results(modules, confdict, seldict, json_results):
    # add module
    # add config
    # add process
    # add results
    logging.info('  Testing results')

    logging.info('    Add module')
    mod_id = dbio.DBModules.create(**modules[0])#upload=False

    logging.info('    Add config')
    conf_id = dbio.DBModuleConfigs.create(**confdict)
    
    logging.info('    Add selector')
    sel_id = dbio.DBSelectors.create(**seldict)

    logging.info('    Add process')
    proc_id = dbio.DBProcesses.create(**{
        'selector':sel_id,
        'data_id': 'not_a_study_id',
        'processstatusname':'new',
        'datasourcename': 'WADQC'
         })

    logging.info('    Add processinglog')
    proc_id.process_log = 'that was easy'
    proc_id.save()
    
    logging.info('    Set process status')
    proc_id.process_status = dbio.DBProcessStatus.get_by_name('busy')
    proc_id.save()
    
    logging.info('    Get process by id')
    p_ids = dbio.DBProcesses.select().where(dbio.DBProcesses.id == proc_id)

    logging.info('    Get process by status')
    p_ids = dbio.DBProcesses.select().join(dbio.DBProcessStatus).where(dbio.DBProcessStatus.name == 'busy')
    if not len(p_ids) == 1:
        logging.info('        ERROR! Found %d (1) processes with status "busy"'%len(p_ids))
    else:
        logging.info('        Good: Found %d (1) processes with status "busy"'%len(p_ids))
    
    logging.info('    Finishing process')
    res_id = dbio.DBResults.finishedProcess(proc_id)
    
    logging.info('    Store results')
    for res in json_results:
        cat = res['category']
        field_dict = res.copy()
        del field_dict['category']
        field_dict['result'] = res_id
        if cat == 'string':
            dbio.DBResultStrings.create(**field_dict)
        elif cat == 'float':
            dbio.DBResultFloats.create(**field_dict)
        elif cat == 'bool':
            dbio.DBResultBools.create(**field_dict)
        elif cat == 'object':
            value = None
            with open(res['val'],'rb') as f:
                value = bytes(f.read())
            field_dict['val'] = bytes_as_string(value)

            dbio.DBResultObjects.create(**field_dict)

    logging.info('    Retrieve results')
    results = dbio.DBResults.get_by_id(res_id).getResults()

    for i,obj in enumerate(results):
        if not isinstance(obj, dbio.DBResultObjects):
            continue
        outname = obj.name+'_'+str(i).zfill(3)+obj.filetype
        with open(outname,'wb') as f:
            #f.write(bytes(string_as_bytes(obj['val']))) # not needed for peewee!
            f.write(obj.val)
        logging.info('      Written %s'%outname)

        import filecmp
        for res in json_results:
            if res['category'] == 'object':
                inname = res['val']
        equal = filecmp.cmp(inname,outname)
        if equal:
            logging.info('      Good! Input and output objects are identical')
            os.remove(outname)
        else:
            logging.info('ERROR! input and output objects are NOT indentical')

    logging.info('    Delete results by pk')
    dbio.DBResults.get_by_id(res_id).delete_instance(recursive=True)

    logging.info('    Delete process by pk')
    try:
        dbio.DBProcesses.get_by_id(proc_id).delete_instance()
    except Exception as e:
        logging.info(' '.join(['      Good: Failed to delete process because it was finished,',str(e)]))

    logging.info('    Delete selector by pk')
    dbio.DBSelectors.get_by_id(sel_id).delete_instance()

    logging.info('    Delete config by pk')
    dbio.DBModuleConfigs.get_by_id(conf_id).delete_instance()

    logging.info('    Delete module by pk')
    dbio.DBModules.get_by_id(mod_id).delete_instance()


if __name__ == "__main__":
    # testing of dbio
    """
    1. open a db (if it does not exist, create it) with correct tables
    """
    rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder
    inifile = os.path.join(rootfolder,'wadconfig.ini') # for testing, use the test config
    setupfile = os.path.join(rootfolder,'wadsetup.ini') # for testing, use the test config
    #inifile = os.path.join(rootfolder,'wadconfig_postgresql.ini') # for testing, use the test config
    
    test_base.reset_logging('INFO')

    logging.info('Fresh start')
    test_base.dbfreshstart(dbio, inifile, setupfile)
    
    tests = ['module','config','selector','rule', 'results', 'testdb']
    
    modules = test_base.multidict(test_base.moddict, 3, ignore=['uploadfilepath', 'filename', 'origin'])
    test_base.confdict['modulename'] = modules[0]['name']

    if 'module' in tests:
        test_module(modules)
        
    if 'config' in tests:
        test_config(modules, test_base.confdict)

    if 'selector' in tests:
        test_selector(modules, test_base.confdict, test_base.seldict)

    if 'rule' in tests:
        test_rule(modules, test_base.confdict, test_base.seldict, test_base.ruledict)

    if 'results' in tests:
        test_results(modules, test_base.confdict, test_base.seldict, test_base.json_results)

    if 'testdb' in tests:
        make_testdb(modules, test_base.confdict, test_base.seldict, test_base.ruledict, test_base.json_results)

    logging.info("all done!")