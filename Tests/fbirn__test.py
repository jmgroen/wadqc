#!/usr/bin/env python
"""
stand alone test for all functions in pacsio
"""
# add parent folder to search path for modules
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import os
import tempfile
import shutil

from pacsio import PACSIO
__version__ = '20160406'

def fbirntest(inifile):
    # attempt to download huge dataset
    import sys
    pacsio = PACSIO(inifile)
    studyid = '1d59fd94-5baf674f-2aa05a17-eeb13c0a-518effe5'
    print pacsio.getSeriesIds(studyid)
    print pacsio.getStudyData(studyid,'/home/aschilha/temp/NOW')
    sys.exit()

if __name__ == "__main__":
    """
    1. ensure Othanc is running and accessable with the provided credentials at the provided URL
    2. upload a dicom folder
    3. delete an instance
    """
    inifile = 'wadconfig_test.ini'
    
    fbirntest(inifile)
