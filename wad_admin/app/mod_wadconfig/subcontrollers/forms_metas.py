# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, FileField, HiddenField, SelectField # BooleanField

# Import Form validators
from wtforms.validators import Required, NoneOf

class ModifyForm(FlaskForm):
    metafile = FileField('Meta file', [ ])  
    
    