# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, DateField, BooleanField, SelectField # 

# Import Form validators
from wtforms.validators import Required, NoneOf, Optional

todo_choices = [(0, 'test filter'), (1, 'download selection'), (2, 'delete selection')]

class QRDForm(FlaskForm):
    sourcename = StringField('source contains', validators=(Optional(),) )
    #datatype = StringField('datatype', [])
    modality   = StringField('modality contains', validators=(Optional(),) )
    seriesdesc = StringField('series desc contains', validators=(Optional(),) )
    protocol   = StringField('protocol contains', validators=(Optional(),) )
    station    = StringField('station contains', validators=(Optional(),) )
    aet        = StringField('aet contains', validators=(Optional(),) )
    seriesdate_min = DateField('start date', format='%Y%m%d', validators=(Optional(),) )
    seriesdate_max = DateField('end date', format='%Y%m%d', validators=(Optional(),) )
    todo = SelectField(choices = todo_choices, coerce=int)
    confirm = BooleanField('confirm delete')
    
    