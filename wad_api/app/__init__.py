#imports
from flask import Flask, jsonify, request, Response, send_file
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, jwt_optional
import os
from datetime import datetime,timedelta
import json
import jsmin
import codecs
import base64
from functools import wraps
from werkzeug import check_password_hash, generate_password_hash
from wad_qc.connection.dbio_models import *
from wad_admin.app.mod_wadconfig.models import WAUsers, role_names

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

#connection info to WAD DB
INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

# Define the WSGI application object
flask_app = Flask(__name__)
flask_app.config['SECRET_KEY']='zMzGXiAqnUl0LOypNoxJCzp8UMrCSOcL'
jwt = JWTManager(flask_app)
flask_app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=5)
flask_app.config['JWT_HEADER_TYPE'] = 'JWT'

# db connection
def get_dict_from_inifile(inifile):
    # only needed if the class is initialized with a inifile instead of a dict read the inputconfig as a config, and
    # cast it into a dict
    config = configparser.SafeConfigParser()
    with open(inifile,'r') as f:
        config.readfp(f)
    config_dict = {}
    for section in config.sections():
        config_dict[section] = {}
        for option in config.options(section):
            config_dict[section][option.upper()] = config.get(section, option)
    return config_dict
dbconfig = get_dict_from_inifile(INIFILE)['iqc-db']

from playhouse.pool import PooledPostgresqlDatabase
database = PooledPostgresqlDatabase(dbconfig['DBASE'],max_connections=32,stale_timeout=300,user=dbconfig['USER'], password=dbconfig['PSWD'], host=dbconfig['HOST'],port=dbconfig['PORT'])

# open connection to database if needed
@flask_app.before_request
def before_request():
    if database.is_closed():
        database.connect()

# close the database connection after we are done
@flask_app.teardown_appcontext
def td(exception):
    if not database.is_closed():
        database.close()
 
# to enable CORS
@flask_app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin','*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response

# helper functions
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

# list of things to use as true boolean
truelist=[1,'1','yes','y',True,'True','true']
 
# database helpers
def get_selectors():
    selectors = []
    rows=DBSelectors.select().order_by(DBSelectors.name)
    for row in rows:
        selectors.append({
            'id':row.id,
            'name':row.name,
            'isactive':row.isactive,
            'description':row.description
            })
    return selectors

def get_results_for_selector(id_selector):
    selector = DBSelectors.get(DBSelectors.id==id_selector)
    results = []
    for result in selector.results:
        if len(result.datetimes)>0:
            results.append({'id':result.id, 'date':max([datetime.val for datetime in result.datetimes])})
        else:
            results.append({'id':result.id, 'date':result.created_time})
    return results

def get_result_for_selector(id_result):
    result = DBResults.get(DBResults.id==id_result)
    meta = json.loads(jsmin.jsmin(bytes_as_string(result.selector.module_config.meta.val)))
    tests = []
    if len(result.datetimes)>0:
        for item in result.datetimes:
            test = get_status_for_test(item.id,'datetime')
            tests.append(test)
    if len(result.floats)>0:
        for item in result.floats:
            test = get_status_for_test(item.id,'float')
            tests.append(test)
    if len(result.strings)>0:
        for item in result.strings:
            test = get_status_for_test(item.id,'string')
            tests.append(test)
    if len(result.objects)>0:
        for item in result.objects:
            t = item.val
            base64EncodedStr = base64.b64encode(t)
            tests.append({'name':item.name,
                             'value':base64EncodedStr,
                             'limit':'', 'status':0,
                             'type':'object',
                             'id':item.id})
    return tests

def get_status_for_test(id_test, rtype):
    units = ''
    display_name = ''
    if rtype=='float':
        test = dbio.DBResultFloats.get(dbio.DBResultFloats.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_minlowhighmax' in meta['results'][test.name]:
                limits = meta['results'][test.name]['constraint_minlowhighmax']
                limit = limits
                if not limits[3] is None and test.val>limits[3]:
                    status = 3
                elif not limits[0] is None and test.val<limits[0]:
                    status = 3
                elif not limits[2] is None and test.val>limits[2]:
                    status = 2
                elif not limits[1] is None and test.val<limits[1]:
                    status = 2
                else:
                    status = 1

            elif 'constraint_refminlowhighmax' in meta['results'][test.name]:
                limits = meta['results'][test.name]['constraint_refminlowhighmax']
                if limits[0] is None:
                    status = 0
                    limit = None
                else:
                    limit = [(limits[0]+limits[0]*(float(limits[1])/100)) if not limits[1] is None else None, 
                             (limits[0]+limits[0]*(float(limits[2])/100)) if not limits[2] is None else None, 
                             (limits[0]+limits[0]*(float(limits[3])/100)) if not limits[3] is None else None, 
                             (limits[0]+limits[0]*(float(limits[4])/100)) if not limits[4] is None else None]
                
                    if not limit[3] is None and test.val>limit[3]:
                        status = 3
                    elif not limit[0] is None and test.val<limit[0]:
                        status = 3
                    elif not limit[2] is None and test.val>limit[2]:
                        status = 2
                    elif not limit[1] is None and test.val<limit[1]:
                        status = 2
                    else:
                        status = 1

            else:
                status = 0
                limit = None

            if 'units' in meta['results'][test.name]:
                units = meta['results'][test.name]['units']
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            status = 0
            limit = None

    elif rtype=='bool':
        test=DBResultBools.get(DBResultBools.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_equals' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_equals']
                if limit==test.val:
                    status = 1
                else:
                    status = 3
            else:
                limit = None
                status = 0
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            limit = None
            status = 0

    elif rtype=='datetime':
        test = DBResultDateTimes.get(DBResultDateTimes.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_period' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_period']
                t = (datetime.now()-test.val)
                t = t.days+t.seconds/60/60/24
                if t>limit:
                    status = 3
                else:
                    status = 1
            else:
                limit = None
                status = 0
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            limit = None
            status = 0

    elif rtype=='string':
        test = DBResultStrings.get(DBResultStrings.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val=test.val
        if test.name in meta['results']:
            if 'constraint_equals' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_equals']
                if limit==test.val:
                    status = 1
                else:
                    status = 3
            else:
                limit=None
                status = 0
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            limit = None
            status = 0
    elif rtype=='object':
        test=DBResultObjects.get(DBResultObjects.id==id_test)
        t = test.val
        val = base64.b64encode(t)
        limit = None
        status = 0
        if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
    else:
        print(id_test, rtype)
    return {'name':test.name,'id':test.id,'type':rtype,'limit':limit,
            'status':status,'value':val,'units':units,'display_name':display_name}

def get_status_for_result(result):
    status = {}
    status_list = []
    for item in result:
        if item['type']=='datetime':
            status['datetime'] = item['status']
        else:
            status_list.append(item['status'])
    try:
        status['tests'] = max(status_list)
    except:
        status['tests'] = 0
    return status

def get_datetime_for_result(result):
    dt = None
    for item in result:
        if item['type']=='datetime':
            dt = item['value']
    return dt

def get_selector(id_selector):
    selector = DBSelectors.get(DBSelectors.id==id_selector)
    return selector

def get_result(id_result):
    result = DBResults.get(DBResults.id==id_result)
    return result

def get_data_for_result(id_result):
    result = get_result(id_result)
    data_id = result.data_id
    data_type = result.module_config.data_type.name
    return (data_type, data_id)

# route functions
def _login(username, password):
    """
    Use WAUsers to authenticate.
    First check password, then check if REST access is granted
    """

    try:
        user = WAUsers.get(WAUsers.username==username)
        if user:
            if check_password_hash(user.password, password):
                if user.status == 0:
                    return False
                return True
            else:
                return False
        else:
            return False
    except WAUsers.DoesNotExist:
        return False

# role_names = {100: 'admin', 500: 'rest_full', 600: 'rest_major', 700: 'rest_minor'}

def get_current_user_role(username):
    try:
        user = WAUsers.get(WAUsers.username==username)
        if user:
            return role_names.get(user.role)
    except WAUsers.DoesNotExist:
        return False

def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        @jwt_optional
        def wrapped(*args,**kwargs):
            valid_user = False
            valid_role = False
            if 'Authorization' in request.headers:
                if request.headers['Authorization'].startswith("JWT"):
                    username = get_jwt_identity()
                    if get_current_user_role(username) not in roles:
                        return jsonify({'success':False, 'msg':"User not authorized!"})
                    else:
                        return f(*args,**kwargs)
                elif request.headers['Authorization'].startswith("Basic"):
                    username = (request.authorization["username"])
                    password = (request.authorization["password"])
                    valid_login = _login(username,password)
                    if valid_login:
                        if get_current_user_role(username) not in roles:
                            return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="User not authorized!"'})
                        else:
                            return f(*args,**kwargs)
            else:
                return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="Authentication Required"'})
        return wrapped
    return wrapper

# Routes catch all undefined routes
@flask_app.route('/', defaults={'path': ''}, methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
@flask_app.route('/<path:path>', methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
def catch_all(path):
    return jsonify({'success':False, 'msg':request.method+ " is not a valid method for '{}'.".format(path)})

### routes for authentication ###
@flask_app.route('/api/authenticate', methods=['POST'])
def login():
    if len(request.form)>0:
        username=request.form['username']
        password=request.form['password']
    elif len(request.get_json())>0:
        username=request.get_json()['username']
        password=request.get_json()['password']
    else:
        return jsonify({"success":False,"msg":"Input should be POST data or JSON data."})

    if not username:
        return jsonify({"success":False, "msg":"No user given"}), 400
    if not password:
        return jsonify({"success":False, "msg":"No password given"}), 400
    login_response = _login(username, password)
    if login_response:
        access_token = create_access_token(identity=username)
        return jsonify({'token':access_token, 'success':True,'admin':get_current_user_role(username)=='admin'})
    else:
        return jsonify({"success":False,'msg':'Wrong user or password'})

@flask_app.route('/api/verifytoken', methods=['POST'])
@jwt_required
def verifytoken():
    user = get_jwt_identity()
    if get_jwt_identity():
        return jsonify({"success":True, "msg":"Welcome back","admin":get_current_user_role(user)=='admin'})
    else:
        return jsonify({"success":False, "msg":"Invalid or no token"})


### routes for data display (mostly GET routes) ###
@flask_app.route('/api/selectors',methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor')
def route_selectors_get():
    selectors = get_selectors()
    return jsonify({'success':True, 'selectors':selectors})

@flask_app.route('/api/selectors/<int:id_selector>', methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor') 
def route_selectors_id_get(id_selector):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    return jsonify({'success':True,
                    'selector':{
                        'id':selector.id,
                        'name':selector.name,
                        'isactive':selector.isactive,
                        'description':selector.description,
                        'id_config':selector.module_config.id,
                        'id_module':selector.module_config.module.id,
                        'id_meta':selector.module_config.meta.id}
                    })

@flask_app.route('/api/selectors/<int:id_selector>/results', methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor')
def route_get_selector_id_results(id_selector):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    selector = get_selector(id_selector)
    results = get_results_for_selector(id_selector)
    return jsonify({'success':True, 'results':results,'selector':{'id':selector.id,'name':selector.name}})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor')
def route_get_selector_id_results_id(id_selector,id_result):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})

    try:
        result=DBResults.get_by_id(id_result)
    except DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)})
    if not selector:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)})
        #we should check whether the result belongs to the selector?

    if request.method=='GET':
        selector = get_selector(id_selector)
        result = get_result_for_selector(id_result)
        status = get_status_for_result(result)
        dt = get_datetime_for_result(result)
        return jsonify({'success':True,
                        'result':{'id':id_result, 'status':status, 'date':dt},
                        'tests':result,
                        'selector':{'id':selector.id, 'name':selector.name}
						})
						
@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/dicom',methods=['GET'])
@required_roles('admin','rest_full','rest_major')
def route_get_data_for_result(id_selector, id_result):
    (data_type, data_id) = get_data_for_result(id_result)
    return jsonify({'success':True, 'data_type':data_type, 'data_id':data_id})

@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor')
def route_get_selector_id_results_last(id_selector):
    selector = get_selector(id_selector)
    results = get_results_for_selector(id_selector)
    if len(results) == 0:
        return jsonify({'success':True,
                        'result':{},
                        'tests':None,
                        'selector':{'id':selector.id, 'name':selector.name}
						})
    
	last_result = results[0]
    for result in results:
        if result['date']>last_result['date']:
            last_result = result
    id_result = last_result['id']
    date_result = last_result['date']
    result = get_result_for_selector(id_result)
    status = get_status_for_result(result)
    return jsonify({'success':True,
                    'result':{'id':id_result, 'date':date_result, 'status':status},
                    'tests':result,
                    'selector':{'id':selector.id, 'name':selector.name}
					})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>',methods=['GET'])
@required_roles('admin','rest_full','rest_major','rest_minor')
def route_get_selector_id_results_id_tests_id_type(id_selector, id_result, id_test, rtype):
    selector = get_selector(id_selector)
    result = get_result_for_selector(id_result)
    status = get_status_for_result(result)
    dt = get_datetime_for_result(result)
    test = get_status_for_test(id_test, rtype)
    return jsonify({'success':True,
                    'selector':{'id':selector.id, 'name':selector.name},
                    'result':{'id':id_result, 'status':status, 'date':dt},
                    'test':test
					})

@flask_app.route('/api/results', methods=['GET'])
@required_roles('admin','rest_full')
def results():
    if request.method=='GET':
        results=[]
        rows=DBResults.select().order_by(DBResults.id.desc())
        for row in rows:
            results.append({
                'id':row.id,
                'selector':{'name':row.selector.name,'id':row.selector.id},
                'module_config':{'id':row.module_config.id,'name':row.module_config.name},
                'data_id':row.data_id,
                'data_type':row.module_config.data_type.name,
                'data_source':row.data_source.name,
                'created_time':row.created_time
            })
        return jsonify({'success':True,
						'results':results
						})

@flask_app.route('/api/results/<int:id_result>', methods=['GET','DELETE'])
@required_roles('admin','rest_full')
def result(id_result):
    try:
        result = DBResults.get_by_id(id_result)
    except DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'No result with id '+id_result})
    if not result:
        return jsonify({'success':False,'msg':'No result with id '+id_result})

    if request.method=='GET':
        row = DBResults.get_by_id(id_result)
        result={
            'id':row.id,
            'selector':{'name':row.selector.name,'id':row.selector.id},
            'data_id':row.data_id,
            'module_config':{'id':row.module_config.id,'name':row.module_config.name},
            'data_type':row.module_config.data_type.name,
            'data_source':row.data_source.name,
            'created_time':row.created_time
            }
        return jsonify({'success':True,'result':result})

    if request.method=='DELETE':
        try:
            result.delete_instance(recursive=True)
        except Exception as e:
            return jsonify({'success':False,'msg':'Error: '+str(e)})
        return jsonify({'success':True,'msg':'Result '+str(id_result)+' deleted.'})						

### routes for WAD-QC administration ###
@flask_app.route('/api/selectors', methods=['POST'])
@required_roles('admin','rest_full')
def route_selectors_post():
    new_selector={}

    existingnames = [v.name for v in DBSelectors.select()]

    if request.form['name'] in existingnames:
        return jsonify({'success':False,'msg':'Selector with this name already exists.'})

    new_selector['name']=request.form['name']
    new_selector['description']=request.form['description']
    if request.form['isactive']=='1':
        new_selector['isactive']=True
    else:
        new_selector['isactive']=False
    new_selector['module_config']=request.form['id_config']

    id_new_selector=DBSelectors.create(**new_selector)
    return jsonify({'success':True,'msg':'Selector '+ new_selector['name'] +' added with id'+str(id_new_selector),'id':id_new_selector})

@flask_app.route('/api/selectors/<int:id_selector>', methods=['DELETE','PUT'])
@required_roles('admin','rest_full')
def route_selectors_id_del_put(id_selector):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})

    if request.method=='DELETE':
        with db.atomic() as trx:
            try:
                DBSelectors.get_by_id(id_selector).delete_instance(recursive=True)
            except IntegrityError as e:
                trx.rollback()
                msg = str(e)
                if "violates foreign key constraint" in str(e):
                    return jsonify({'success':False,'msg':'This Selector is referenced from a table from an external service (e.g. WAD Dashboard). Use that service to remove the reference and try again.'})
        return jsonify({'success':True,'msg':'Selector deleted'})

    if request.method=='PUT':
        other_existingnames = [v.name for v in DBSelectors.select().where(DBSelectors.id != id_selector)]

        if 'name' in request.form:
            if request.form['name'] in other_existingnames:
                return jsonify({'success':False,'msg':'There already is a selector with the name '+str(request.form['name'])})
            selector.name=request.form['name']

        if 'description' in request.form:
            selector.description=request.form['description']

        truelist=[1,'1','yes','y',True,'True','true']

        if 'isactive' in request.form:
            if request.form['isactive'] in truelist:
                selector.isactive=True
            else:
                selector.isactive=False

        if 'id_config' in request.form:
            selector.module_config=request.form['id_config']

        selector.save()

        return jsonify({'success':True,'msg':'Selector '+ selector.name +' updated'})

@flask_app.route('/api/selectors/<int:id_selector>/rules',methods=['GET','POST'])
@required_roles('admin','rest_full')
def selector_rules(id_selector):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if request.method=='GET':
        rules=[]
        selector=get_selector(id_selector)
        for rule in selector.rules:
            selectorrule={}
            selectorrule['id']=rule.id
            selectorrule['dicomtag']=rule.dicomtag
            selectorrule['logic']=rule.logic.name
            selectorrule['values']=[]
            for item in rule.values:
                selectorrule['values'].append(item.val)
            rules.append(selectorrule)
        return jsonify({'success':True,'rules':rules})
    if request.method=='POST':
        values=request.form['values']
        values=values.split(";")
        try:
            selector.addRule(request.form['dicomtag'],request.form['logic'],values)
            return jsonify({'success':True,'msg':'Rule added'})
        except Exception as e:
            return jsonify({'success':False,'msg':e.message})

@flask_app.route('/api/selectors/<int:id_selector>/rules/<int:id_rule>',methods=['PUT','DELETE'])
@required_roles('admin','rest_full')
def selector_rules_id(id_selector,id_rule):
    try:
        rule = DBSelectorRules.get_by_id(id_rule)
    except DBSelectorRules.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)})
    if not rule:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)})
    if request.method=='DELETE':
        try:
            DBSelectorRules.get_by_id(id_rule).delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Rule deleted'})
        except Exception as e:
            return jsonify({'success':False,'msg':e.message})

    if request.method=='PUT':
        rule.dicomtag=request.form['dicomtag']
        rule.logic=request.form['logic']
        rule.save()

        try:
            d = DBRuleValues.delete().where(DBRuleValues.rule==rule.id)
            d.execute()

            values=request.form['values']
            values=values.split(";")

            for val in values:
                DBRuleValues.create(rule=rule, val=val)
            return jsonify({'success':True,'msg':'Rule updated'})

        except Exception as e:
            return jsonify({'success':False,'msg':e.message})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['DELETE'])
@required_roles('admin','rest_full')
def route_del_selector_id_results_id(id_selector, id_result):
    try:
        selector=DBSelectors.get_by_id(id_selector)
    except DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)})

    try:
        result=DBResults.get_by_id(id_result)
    except DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)})
    if not selector:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)})
    if request.method=='DELETE':
        try:
            result.delete_instance(recursive=True)
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)})
        return jsonify({'success':True,'msg':'Result with id '+str(id_result)+' deleted'})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>',methods=['DELETE','PUT'])
@required_roles('admin','rest_full')
def route_del_selector_id_results_id_tests_id_type(id_selector, id_result, id_test, rtype):
    if rtype=='float':
        test = DBResultFloats.get(DBResultFloats.id==id_test)
    elif rtype=='bool':
        test=DBResultBools.get(DBResultBools.id==id_test)
    elif rtype=='datetime':
        test = DBResultDateTimes.get(DBResultDateTimes.id==id_test)
    elif rtype=='string':
        test = DBResultStrings.get(DBResultStrings.id==id_test)
    elif rtype=='object':
        test=DBResultObjects.get(DBResultObjects.id==id_test)
    if not test:
        return jsonify({'success':False,'msg':'No test with id '+str(id_test)+' of type '+rtype})

    if request.method=='DELETE':
        try:
            test.delete_instance()
        except Exception as e:
            return jsonify({'success':False,'msg':e})
        return jsonify({'success':True,'msg':'Test with id '+str(id_test)+' of type '+rtype+' deleted'})
    if request.method=='PUT':
        try:
            test.val=request.form['val']
            test.save()
        except Exception as e:
            return jsonify({'success':False,'msg':e})
        return jsonify({'success':True,'msg':'Test with id '+str(id_test)+' of type '+rtype+' updated'})
		
@flask_app.route('/api/users', methods=['GET','POST'])
@required_roles('admin','rest_full')
def users():
    if request.method=='GET':
        users = []
        rows = WAUsers.select()
        for row in rows:
            users.append({
            'id':row.id,
            'name':row.username,
            'email':row.email,
            'status':row.status,
            'role':role_names.get(row.role)
            })
        return jsonify({'success':True, 'users':users})
    if request.method=='POST':
        username=request.form['username']
        usercheck = WAUsers.select().where(WAUsers.username==username)
        if usercheck.exists():
            return jsonify({'success':False, "msg":"User already exists"})
        password=generate_password_hash(request.form['password'])
        email=request.form['email']
        role=request.form['role']
        try:
            user = WAUsers.create(username=username, password=password, email=email, status=1, role=role, refresh=30)
            return jsonify({'success':True, 'msg':'Account for '+username+' created','id':user.id})
        except:
            return jsonify({'success':False, "msg":"Something went wrong"})

@flask_app.route('/api/users/<int:id_user>', methods=['DELETE','PUT'])
@required_roles('admin','rest_full')
def users_edit_del(id_user):
    try:
        user=WAUsers.get_by_id(id_user)
    except WAUsers.DoesNotExist:
        return jsonify({'success':False,'msg':'No user with id '+str(id_user)})
    if not user:
        return jsonify({'success':False,'msg':'No user with id '+str(id_user)})
    if request.method=='PUT':
        user.username=request.form['username']
        user.password=generate_password_hash(request.form['password'])
        user.email=request.form['email']
        user.role=request.form['role']
        user.status=request.form['status']
        user.save()
        return jsonify({'success':True, 'msg':'Account for '+user.username+' updated','id':user.id})

    if request.method=='DELETE':
        user.delete_instance()
        return jsonify({'success':True, 'msg':'Account for '+user.username+' deleted'})

from wad_admin.app.libs.shared import upload_file

@flask_app.route('/api/modules',methods=['GET','POST'])
@required_roles('admin','rest_full')
def modules():
    if request.method=='GET':
        modules=[]
        rows = DBModules.select().order_by(DBModules.id)
        for row in rows:
            modules.append({
                'id': row.id,
                 'name': row.name,
                 'description':row.description,
                 'executable':row.filename
            })
        return jsonify({'success':True,'modules':modules})

    if request.method=='POST':

        new_module={}

        existingnames = [v.name for v in DBModules.select()]

        if request.form['name'] in existingnames:
            return jsonify({'success':False,'msg':'A module with this name already exists.'})
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            if not outname is None:
                new_module['uploadfilepath'] = outname
        else:
            return jsonify({'success':False,'msg':'No file uploaded!'})

        new_module['name'] = request.form['name']
        new_module['description'] = request.form['description']
        new_module['filename'] = os.path.basename(request.form['executable'])
        new_module['origin']='user'
        new_module['repo_version']=''
        new_module['repo_url']=''
        id_new_module=DBModules.create(**new_module)
        return jsonify({'success':True,'msg':'Module '+ request.form['name'] +' added.','id':id_new_module})

@flask_app.route('/api/modules/<int:id_module>',methods=['DELETE','PUT'])
@required_roles('admin','rest_full')
def modules_id(id_module):
    try:
        mod = DBModules.get_by_id(id_module)
    except DBModules.DoesNotExist:
        return jsonify({'success':False,'msg':'No module with that id'})
    if not mod:
        return jsonify({'success':False,'msg':'No module with that id'})
    # if request.method='GET':
        # row = DBModuleConfigs.get_by_id(id_config)
            # if 'download' in request.args:
                # if request.args['download'] in truelist:

    if request.method=='DELETE':
        sels = sum([1 if len(cfg.selectors) else 0 for cfg in mod.module_configs ])
        if not sels == 0:
            return jsonify({'success':False,'msg':'Module is coupled with '+sels+' selectors, please delete these selectors first.'})

        modname = mod.name

        del_meta = [ c.meta for c in mod.module_configs ]
        mod.delete_instance(recursive=True)

        for mt in del_meta:
            try:
                mt.delete_instance(recursive=False)
            except:
                pass

        return jsonify({'success':True,'msg':'Module '+modname+' deleted.'})
    if request.method=='PUT':
        try:
            module = DBModules.get_by_id(id_module)
        except DBModules.DoesNotExist:
            return jsonify({'success':False,'msg':'No module with that id'})
        if not module:
            return jsonify({'success':False,'msg':'No module with that id'})
        other_existingnames = [v.name for v in DBModules.select().where(DBModules.id != id_module)]

        if request.form['name'] in other_existingnames:
            return jsonify({'success':False,'msg':'A module with this name already exists.'})
        outname = None
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            print('outname: ',outname)
            if not outname is None:
                uploadfilepath = outname

        module.name = request.form['name']
        module.description = request.form['description']
        module.filename = os.path.basename(request.form['executable'])
        # make clear that this module is not a factory module anymore
        module.origin = 'user'
        module.repo_version = ""
        module.repo_url = ""
        if not outname is None:
            module.save(uploadfilepath=outname)
        else:
            module.save()

        if not outname is None:
            os.remove(outname)

        return jsonify({'success':True,'msg':'Module '+ module.name +' updated.'})

from wad_admin.app.libs import configmaintenance

@flask_app.route('/api/configs',methods=['GET','POST'])
@required_roles('admin','rest_full')
def configs():
    if request.method=='GET':
        configs=[]
        rows = DBModuleConfigs.select().order_by(DBModuleConfigs.id)
        for row in rows:
            configs.append({
                'id': row.id,
                'name': row.name,
                'description':row.description,
                'module':row.module.name,
                'data_type':row.data_type.name
            })
        return jsonify({'success':True,'configs':configs})

    if request.method=='POST':
        print(request.form)
        print(request.files)
        new_config={}

        existingnames = [v.name for v in DBModuleConfigs.select()]

        if request.form['name'] in existingnames:
            return jsonify({'success':False,'msg':'A config with this name already exists.'})
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            if not outname is None:
                try:
                    with open(outname,'r') as fcfg: blob = fcfg.read()
                except Exception as e:
                    return jsonify({success:False,'msg':'Not a valid config.json.'})
                os.remove(outname)
            if blob:
                valid, msg = configmaintenance.validate_json('configfile', blob)
                if not valid:
                    return jsonify({success:False,'msg':msg})
            else:
                return jsonify({success:False,'msg':'Not a valid config.json.'})
        else:
            return jsonify({'success':False,'msg':'No file uploaded!'})


        new_config['name'] = request.form['name']
        new_config['description'] = request.form['description']
        new_config['data_type'] = request.form['data_type']
        new_config['module'] = request.form['module']
        new_config['origin']='user'
        new_config['val']=blob

        empty_meta = DBMetaConfigs.create()
        new_config['meta']=empty_meta.id

        id_new_config=DBModuleConfigs.create(**new_config)
        return jsonify({'success':True,'msg':'Config '+ new_config['name'] +' added.','id':id_new_config})

from io import BytesIO

@flask_app.route('/api/configs/<int:id_config>',methods=['DELETE','PUT','GET'])
@required_roles('admin','rest_full')
def configs_id(id_config):
    try:
        config = DBModuleConfigs.get_by_id(id_config)
    except DBModuleConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No config with that id'})
    if not config:
        return jsonify({'success':False,'msg':'No config with that id'})
    if request.method=='GET':
        row = DBModuleConfigs.get_by_id(id_config)
        if 'download' in request.args:
            if request.args['download'] in truelist:
                filename = row.name+'.json'
                blob = row.val
                blob = json.loads(bytes_as_string(blob))
                blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))
                return send_file(BytesIO(blob), as_attachment=True, attachment_filename=filename)
        config=[]
        config.append({
            'id': row.id,
            'name': row.name,
            'description':row.description,
            'module':row.module.name,
            'data_type':row.data_type.name
        })
        return jsonify({'success':True,'config':config})

    if request.method=='DELETE':
        if len(config.selectors) > 0:
            return jsonify({'success':False,'msg':'Config is coupled with '+str(len(config.selectors))+' selectors, please delete these selectors first.'})

        configname=config.name
        config.delete_instance(recursive=True)

        return jsonify({'success':True,'msg':'Config '+configname+' deleted.'})
    if request.method=='PUT':
        other_existingnames = [v.name for v in DBModuleConfigs.select().where(DBModuleConfigs.id != id_config)]

        if request.form['name'] in other_existingnames:
            return jsonify({'success':False,'msg':'A config with this name already exists.'})
        outname = None
        blob = None
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            if not outname is None:
                try:
                    with open(outname,'r') as fcfg: blob = fcfg.read()
                except Exception as e:
                    return jsonify({success:False,'msg':'Not a valid config.json.'})
            if blob:
                valid, msg = configmaintenance.validate_json('configfile', blob)
                if not valid:
                    return jsonify({success:False,'msg':'Not a valid config.json.'})
            else:
                return jsonify({success:False,'msg':'Not a valid config.json.'})

        config.name = request.form['name']
        config.description = request.form['description']
        if not blob is None:
            config.val=blob
        if 'data_type' in request.form:
            config.data_type=request.form['data_type']

        # make clear that this module is not a factory module anymore
        config.origin= 'user'
        config.save()

        if not outname is None:
            os.remove(outname)

        return jsonify({'success':True,'msg':'Config '+ config.name +' updated.'})

@flask_app.route('/api/metas',methods=['GET','POST'])
@required_roles('admin','rest_full')
def metas():
    if request.method=='GET':
        metas=[]
        rows = DBMetaConfigs.select().order_by(DBMetaConfigs.id)
        for row in rows:
            metas.append({
                'id': row.id
                #'name':row.name, 'description':row.description
                # 'val': row.val
            })
        return jsonify({'success':True,'metas':metas})

    if request.method=='POST':
        new_meta={}
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            if not outname is None:
                try:
                    with open(outname,'r') as fcfg: blob = fcfg.read()
                except Exception as e:
                    return jsonify({success:False,'msg':'Not a valid meta.json.'})
                os.remove(outname)
            if blob:
                valid, msg = configmaintenance.validate_json('metafile', blob)
                if not valid:
                    return jsonify({success:False,'msg':msg})
            else:
                return jsonify({success:False,'msg':'Not a valid meta.json.'})
        else:
            return jsonify({'success':False,'msg':'No file uploaded!'})
        new_meta['val']=blob
        new_meta_id=DBMetaConfigs.create(**new_meta)
        return jsonify({'success':True,'msg':'Meta added with id '+new_meta_id,'id':new_meta_id})

@flask_app.route('/api/metas/<int:id_meta>',methods=['DELETE','PUT','GET'])
@required_roles('admin','rest_full')
def metas_id(id_meta):
    try:
        meta = DBMetaConfigs.get_by_id(id_meta)
    except DBMetaConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta})
    if not meta:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta})

    if request.method=='GET':
        row = DBMetaConfigs.get_by_id(id_meta)
        if 'download' in request.args:
            if request.args['download'] in truelist:
                filename = row.name+'.json'
                blob = row.val
                blob = json.loads(bytes_as_string(blob))
                blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))
                return send_file(BytesIO(blob), as_attachment=True, attachment_filename=filename)
        meta=[]
        meta.append({
            'id': row.id
        })
        return jsonify({'success':True,'meta':meta})

    if request.method=='DELETE':
        if len(meta.module_configs)>0 and len(meta.module_configs[0].selectors)>0:
            return jsonify({'success':False,'msg':'Meta is coupled with '+str(len(meta.module_configs[0].selectors))+' selectors, please delete these selectors first.'})

        if len(meta.module_configs)>0:
            return jsonify({'success':False,'msg':'Meta is coupled with '+str(len(meta.module_configs))+' configs, please change the meta for these configs first.'})

        meta.delete_instance(recursive=True)

        return jsonify({'success':True,'msg':'Meta deleted.'})

    if request.method=='PUT':
        if 'file' in request.files and not len(request.files['file'].filename) == 0:
            outname = upload_file(request.files['file'])
            if not outname is None:
                try:
                    with open(outname,'r') as fcfg: blob = fcfg.read()
                except Exception as e:
                    return jsonify({success:False,'msg':'Not a valid meta.json.'})
                os.remove(outname)
            if blob:
                valid, msg = configmaintenance.validate_json('metafile', blob)
                if not valid:
                    return jsonify({success:False,'msg':msg})
            else:
                return jsonify({success:False,'msg':'Not a valid meta.json.'})
        else:
            return jsonify({'success':False,'msg':'No file uploaded!'})
        meta.val=blob
        meta.save()
        return jsonify({'success':True,'msg':'Meta '+str(meta.id)+' updated.'})

import wad_admin.wadservices_communicate as wad_com

@flask_app.route('/api/admin/services', methods=['GET','POST'])
@required_roles('admin','rest_full')
def admin_services():
    if request.method=='GET':
        services=[]
        rows=['wadprocessor','postgresql','Orthanc']

        for row in rows:
            if row == 'wadprocessor':
                stat = wad_com.wadcontrol('status', INIFILE)
                if stat == 'ERROR':
                    stat = 'stopped'
            elif row == 'postgresql':
                stat = wad_com.postgresql('status')
            else:
                stat = wad_com.status(row)
            services.append({
                'name':row,
                'status':stat
            })
        return jsonify({'success':True, 'services':services})
    if request.method=='POST':
        if 'service' in request.form:
            service=request.form['service']
        else:
            return jsonify({'success':False,'msg':'Service should be wadprocessor, postgresql, Orthanc'})
        if 'command' in request.form:
            command=request.form['command']
        else:
            return jsonify({'success':False,'msg':'No command given'})

        if not command in ['start','stop']:
            return jsonify({'success':False,'msg':'Command should be start or stop'})

        if command=='start':
            if service=='wadprocessor':
                r=wad_com.start('wadprocessor',INIFILE)
                print(r)
            elif service=='postgresql':
                r=wad_com.postgresql(command)
            else:
                r=wad_com.start(service,INIFILE) #Orthanc,wadapi
            if r=='OK':
                return jsonify({'success':True,'msg':service+' started'})
        if command=='stop':
            if service=='postgresql':
                r=wad_com.postgresql(command)
            else:
                r=wad_com.stop(service) #Orthanc,wadapi
            if r=='OK':
                return jsonify({'success':True,'msg':service+' stopped'})

@flask_app.route('/api/processes', methods=['GET'])
@required_roles('admin','rest_full')
def processes():
    if request.method=='GET':
        processes=[]
        rows=DBProcesses.select().order_by(DBProcesses.id.desc())
        for row in rows:
            processes.append({
                'id':row.id,
                'selector':{'name':row.selector.name,'id':row.selector.id},
                'data_id':row.data_id,
                'data_type':row.module_config.data_type.name,
                'process_status':row.process_status.name,
                'data_source':row.data_source.name,
                'created_time':row.created_time
            })
        return jsonify({'success':True,'processes':processes})

@flask_app.route('/api/processes/<int:id_process>', methods=['GET','DELETE','PUT'])
@required_roles('admin','rest_full')
def process(id_process):
    try:
        process = DBProcesses.get_by_id(id_process)
    except DBProcesses.DoesNotExist:
        return jsonify({'success':False,'msg':'No proces with id '+id_process})
    if not process:
        return jsonify({'success':False,'msg':'No proces with id '+id_process})

    if request.method=='GET':
        row = DBProcesses.get_by_id(id_process)
        process={
            'log':bytes_as_string(row.process_log),
            'id':row.id,
            'selector':{'name':row.selector.name,'id':row.selector.id},
            'data_source':row.data_source.name,
            'data_id':row.data_id,
            'data_type':row.module_config.data_type.name,
            'process_status':row.process_status.name,
            'created_time':row.created_time
            }
        return jsonify({'success':True,'process':process})

    if request.method=='DELETE':
        try:
            process.delete_instance(recursive=True)
        except Exception as e:
            return jsonify({'success':False,'msg':'Error: '+str(e)})
        return jsonify({'success':True,'msg':'Process '+str(id_process)+' deleted.'})

    if request.method=='PUT':
        try:
            from wad_core.selector import Selector
            sel_name    = process.selector.name
            source_name = process.data_source.name
            data_id     = process.data_id
            datatype_name = process.module_config.data_type.name
            process.delete_instance(recursive=True)

            wsel = Selector(INIFILE, logfile_only=True)
            wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])

        except Exception as e:
            return jsonify({'success':False,'msg':'Error: '+str(e)})

        return jsonify({'success':True,'msg':'Process '+str(id_process)+' send to wadselector again.'})

### routes for the WAD-QC framework ###
@flask_app.route('/api/wadselector', methods=['GET'])
def wadselector():
    import subprocess

    studyid = request.args.get('studyid')
    source = request.args.get('source')

    try:
        src = DBDataSources.get(DBDataSources.name==source)
    except DBDataSources.DoesNotExist:
        return jsonify({'success':False, 'msg':"Unregistered Source '{}'".format(source)})
    selector_wrapper = os.path.join(os.environ['WADROOT'], 'orthanc', 'lua', 'wadselector.py')
    cmd = [selector_wrapper, '--source', source, '--studyid', studyid, '--inifile', INIFILE, '--logfile_only']
    subprocess.Popen(cmd)
    return jsonify({'success':True, 'msg':'Study send to wadselector'})
	
@flask_app.route('/api/wadwriter',methods=['GET','POST'])
#@required_roles()
def WADwriter():
    from wad_api.app.wadwriter import makeds,sendds

    if request.method=='GET':

        request_dict = request.args.to_dict()
        ds=makeds(request_dict)
        response=sendds(ds,'127.0.0.1',11112)

        if response['success']:
            return jsonify({'success':True,'msg':'DICOM report build and send to WADQC'})
        else:
            return jsonify({'success':False,'msg':response['msg']})

    if request.method=='POST':

        data={}
        objects={}

        for item in request.form:
            data[item]=request.form[item]
        for item in request.files:
            outname = upload_file(request.files[item])
            objects[request.files[item].filename]=outname

        ds=makeds(data,objects)

        for item in objects:
            os.remove(objects[item])

        response=sendds(ds,'127.0.0.1',11112)

        if response['success']:
            return jsonify({'success':True,'msg':'DICOM report build and send to WADQC'})
        else:
            return jsonify({'success':False,'msg':response['msg']})

### routes for unit-testing ###
@flask_app.route('/api/testaccess_full', methods=['GET'])
@required_roles('rest_full')
def testaccess_full():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_full access'})

@flask_app.route('/api/testaccess_major',methods=['GET'])
@required_roles('rest_major')
def testaccess_major():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_major access'})

@flask_app.route('/api/testaccess_minor', methods=['GET'])
@required_roles('rest_minor')
def testaccess_minor():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_minor access'})
  
##  to run a test server with commandline output visible and auto restart
if __name__ == "__main__":
    flask_app.run(host='0.0.0.0', port=3001,use_reloader=True)

