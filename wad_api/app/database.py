from peewee import CharField 

#define settings model
class WADadministrator_settings(dbio.DBModel)
	name = CharField(max_length=64)
	val = CharField(max_length=64)
	
WADadministrator_tables = [
	WADadministrator_settings
]

