import os
import tempfile
import datetime as dt
import pydicom
import json
import base64

from pydicom._storage_sopclass_uids import BasicTextSRStorage
from pydicom.dataset import Dataset, FileDataset

WADQCSCHEME = "WADQC"
WADQCCODE_MANUAL_INPUT = ("WAD-QC Manual Input", "WQ-MAN_INP")
WADQCCODE_PARAMS = ("params", "WQ-PARS")
WADQCCODE_OBJECTS = ("objects", "WQ-OBJS")

def _transfer_syntax(use_little_endian, use_implicit_vr):
    """
    helper to determine file_meta TransferSyntaxUID
    """
    if use_little_endian and use_implicit_vr:
        return pydicom.uid.ImplicitVRLittleEndian
    elif use_little_endian and not use_implicit_vr:   
        return pydicom.uid.ExplicitVRLittleEndian
    elif not use_little_endian and not use_implicit_vr:
        return pydicom.uid.ExplicitVRBigEndian
    else:
        return pydicom.uid.ImplicitVRBigEndian

def _concept_name_code_sequence(meaning_value):
    """
    helper to make ConceptNameCodeSequence
    """
    ds = Dataset()

    ds.CodingSchemeDesignator = WADQCSCHEME
    ds.CodeMeaning = meaning_value[0]
    ds.CodeValue = meaning_value[1]
    
    return ds
        
def _content_sequence_params(data):
    """
    helper to make ContentSequence for params
    """
    ds = Dataset()

    ds.ConceptNameCodeSequence = [ _concept_name_code_sequence(WADQCCODE_PARAMS) ]
    ds.RelationshipType = "CONTAINS"
    ds.ValueType = "TEXT"
    ds.TextValue = json.dumps(data)

    return ds

def _content_sequence_object_read(key, value):
    """
    helper to make ContentSequence for object
    """
    ds = Dataset()

    code = Dataset()
    
    ds.ConceptNameCodeSequence = [ _concept_name_code_sequence( ("object::{}".format(key), "WQ-OBJ")) ]
    ds.RelationshipType = "CONTAINS"
    ds.ValueType = "TEXT"
    with open(value, "rb") as fio:
        image = fio.read()
    ds.TextValue = base64.b64encode(image)

    return ds

def _content_sequence_objs_container(objects):
    """
    helper to make ContentSequence for objects container
    """
    ds = Dataset()

    ds.ConceptNameCodeSequence = [ _concept_name_code_sequence(WADQCCODE_OBJECTS) ]
    ds.ValueType = "CONTAINER"
    ds.ContinuityOfContent = "SEPARATE"
    ds.RelationshipType = "CONTAINS"
    ds.ContentSequence = [ _content_sequence_object_read(key, value) for key, value in objects.items() ]

    return ds        
        
def makeds(request_dict):
    suffix = '.dcm'
    filename = tempfile.NamedTemporaryFile(suffix=suffix).name
    
    file_meta = Dataset()
    file_meta.MediaStorageSOPClassUID = BasicTextSRStorage
    file_meta.MediaStorageSOPInstanceUID = pydicom.uid.generate_uid()
    file_meta.ImplementationClassUID = pydicom.uid.generate_uid()
    
    use_little_endian = True
    use_implicit_vr = True
    
    file_meta.TransferSyntaxUID = _transfer_syntax(use_little_endian, use_implicit_vr)
    
    file_meta.ImplementationVersionName = "20190213" 
    file_meta.FileMetaInformationGroupLength = 1 # will be fixed automatically, but needs to be set
    
    ds = FileDataset(None, {}, file_meta=file_meta, preamble=b"\0" * 128)
    ds.Modality = 'SR'
    ds.is_little_endian = use_little_endian
    ds.is_implicit_VR = use_implicit_vr
    ds.read_encoding = "latin1"
    
    # minimal set of UIDS
    ds.SOPClassUID       = file_meta.MediaStorageSOPClassUID
    ds.SOPInstanceUID    = file_meta.MediaStorageSOPInstanceUID
    ds.StudyInstanceUID  = pydicom.uid.generate_uid()
    ds.SeriesInstanceUID = pydicom.uid.generate_uid()
    
    # Add minimal identification, setting standard first
    ds.PatientID = "wad.123456789"
    ds.PatientName = "W.A.D. Writer".replace(" ", "^")
    ds.StudyDescription = "WADwriterStudy"
    ds.SeriesDescription = "WADwriterSerie"
    ds.StationName = "WADwriter"
    ds.StudyDate = "20190213"
    ds.StudyTime = "144000"
    
    # then see if they are defined in the request and overwrite
    obligated_items=['PatientID','PatientName','StudyDescription','SeriesDescription','StationName','StudyDate','StudyTime']
    for item in obligated_items:
        if item in request_dict:
            try:
                setattr(ds, item, request_dict[item])
            except Exception as e:
                print("WADwriter: Could not set identifier {}: {}".format(item, str(e)))
    
    # extra identifiers to comply with dciodvfy
    ds.SeriesNumber   = 2019
    ds.InstanceNumber = 1
    ds.CompletionFlag = "COMPLETE"
    ds.VerificationFlag = "UNVERIFIED"
    ds.PatientBirthDate = "20190329"
    ds.PatientSex = "O"
    ds.ReferringPhysicianName = "WAD-QC Manual Input".replace(" ", "^")
    ds.StudyID = "WAD-QC.012345678"
    ds.AccessionNumber = "WAD-QC.012345678"
    ds.Manufacturer = "WAD-QC"
    ds.ReferencedPerformedProcedureStepSequence = ""
    ds.PerformedProcedureCodeSequence = ""
    
    # Set creation date/time
    now = dt.datetime.now()
    ds.ContentDate = now.strftime('%Y%m%d')
    timeStr = now.strftime('%H%M%S.%f')  # long format with micro seconds
    ds.ContentTime = timeStr
    
    #remove the obligated items from the request_dict
    for item in request_dict.copy():
        if item in obligated_items:
            del request_dict[item]

    
    # populate data
    ds.ValueType = "CONTAINER"
    ds.ContinuityOfContent = "SEPARATE"
    ds.ConceptNameCodeSequence = [_concept_name_code_sequence(WADQCCODE_MANUAL_INPUT) ]
    if objects is None or objects=={}:
        ds.ContentSequence = [ _content_sequence_params(request_dict) ]
    else:
        ds.ContentSequence = [_content_sequence_params(request_dict),_content_sequence_objs_container(objects)]

    return ds
    
def sendds(ds,ip,port):
    from pynetdicom import AE, StoragePresentationContexts, VerificationPresentationContexts
    from pynetdicom.sop_class import VerificationSOPClass
    from pydicom.uid import ExplicitVRLittleEndian, ImplicitVRLittleEndian, ExplicitVRBigEndian
    from pydicom import read_file
    from pydicom.dataset import Dataset

    # Initialise the Application Entity
    ae = AE()

    #Set Transfer Syntax Options
    ts=[
        ImplicitVRLittleEndian,
        ExplicitVRLittleEndian,
        ExplicitVRBigEndian
    ]

    # Add a requested presentation context
    ae.requested_contexts=VerificationPresentationContexts
    ae.add_requested_context('1.2.840.10008.5.1.4.1.1.88.11',transfer_syntax=ts)

    # Associate with WADQC at ip 127.0.0.1 and port 11112
    assoc = ae.associate(ip,port)
    if assoc.is_established:
        # Use the C-STORE service to send the dataset
        # returns a pydicom Dataset
        status = assoc.send_c_store(ds)
        
        # Release the association
        assoc.release()
        # Check the status of the storage request
        if 'Status' in status:
            # If the storage request succeeded this will be 0x0000
            response={'success':True,'msg':'DICOM report build and send to WADQC'}
        else:
            print('Connection timed out or invalid response from peer')
            response={'success':False,'msg':'Time out or invalid response form WADQC'}
    else:
        response={'success':False,'msg':'Could not connect to WADQC'}

    return response


