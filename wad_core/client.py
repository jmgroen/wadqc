#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import Pyro4

def main(): # define separately to allow automatic script creation
    # prevent problems when communicating from python2 client to python3 daemon
    Pyro4.config.SERIALIZER = 'json'
    Pyro4.config.COMMTIMEOUT = 30      # set for all Pyro4 instances to 30 seconds
    method = sys.argv[1]
    arguments = sys.argv[2:]

    file_list = ['pyro_uri']
    if 'WADROOT' in os.environ:
        file_list.append(os.path.join(os.environ['WADROOT'], 'WAD_QC', 'pyro_uri'))

    exc = None
    for furi in file_list:
        try:
            with open(furi, 'r') as f:
                uri = f.read()
            tm = Pyro4.Proxy(uri)
            return_value = getattr(tm, method)(*arguments)
            if return_value: print(str(return_value))
            return
        except Exception as e:
            exc = e
            continue

    if not exc is None:
        print("ERROR! Cannot connect to WADProcessor! {}".format(str(exc)))
        sys.exit(1) # return error status
    
if __name__ == "__main__":
    main()
