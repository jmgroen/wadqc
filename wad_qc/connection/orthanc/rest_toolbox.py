__version__ = '20160527'

"""
Changelog:
  20160527: code clean up; move credentials into pacsio
  20160526: replaced httplib2 with requests (modern, no close connection issues)
  20160414: fixed httplib2 error for all calls
  20160315: AS: fixed httplib2 error on POST broken pipe meaning no credentials
"""
import requests
from requests.auth import HTTPBasicAuth
import base64
import json
import sys

if (sys.version_info >= (3, 0)):
    from urllib.parse import urlencode
else:
    from urllib import urlencode

def _DecodeJson(s):
    # helper function to generate json
    try:
        if (sys.version_info >= (3, 0)):
            return json.loads(s.decode())
        else:
            return json.loads(s)
    except:
        return s


def DoGet(uri, credentials, data = {}, interpretAsJson = True):
    d = ''
    if len(data.keys()) > 0:
        d = '?' + urlencode(data)

    resp = requests.get(uri + d, auth=HTTPBasicAuth(*(credentials)))
    if not (resp.status_code in [ 200 ]):
        raise Exception(resp.status_code)
    elif not interpretAsJson:
        return resp.content.decode()
    else:
        return _DecodeJson(resp.content)

def _prepareContentForSending(data, contentType):
    # setup the proper headers and encapsulation of data for put/post requests
    if isinstance(data, bytes):
        body = data
        if len(contentType) != 0:
            headers = {'Content-Type': contentType}
        else:
            headers = {'Content-Type': 'text/plain'}
    else:
        body = json.dumps(data)
        headers = {'Content-Type': 'application/json'}
    return headers, body

def DoPost(uri, credentials, data = {}, contentType = ''):
    headers, body = _prepareContentForSending(data, contentType)
    resp = requests.post(
        uri,
        data = body,
        auth = HTTPBasicAuth(*(credentials)),
        headers = headers)

    if not (resp.status_code in [ 200, 302 ]):
        raise Exception(resp.status_code)
    else:
        return _DecodeJson(resp.content)

def DoPut(uri, credentials, data = {}, contentType = ''):
    headers, body = _prepareContentForSending(data, contentType)
    resp = requests.put(
        uri,
        body = body,
        auth = HTTPBasicAuth(*(credentials)),
        headers = headers)

    if not (resp.status_code in [ 200, 302 ]):
        raise Exception(resp.status_code)
    else:
        return _DecodeJson(resp.content)

def DoDelete(uri, credentials):
    resp = requests.delete(
        uri, 
        auth=HTTPBasicAuth(*(credentials)))

    if not (resp.status_code in [ 200 ]):
        raise Exception(resp.status_code)
    else:
        return _DecodeJson(resp.content)


